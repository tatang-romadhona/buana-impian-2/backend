const file = require('./media/file');
const image = require('./media/image');
const auth = require('./auth');

module.exports = {
    file,
    image,
    auth
};