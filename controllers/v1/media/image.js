const { imagekit } = require('../../../util');
const { Image } = require('../../../models');

module.exports = {
    store: async (req, res, next) => {
        try {
            const { originalname, mimetype, buffer } = req.file;
            const file = buffer.toString('base64');
            // const fileName = Date.now() + '.' + originalname.split('.')[1];

            const { fileId, name, thumbnailUrl, url } = await imagekit.upload(originalname, file);

            const image = await Image.create({
                file_id: fileId,
                file_name: name,
                thumbnail_url: thumbnailUrl,
                url: url,
            });

            res.status(201).json({
                status: true,
                message: 'ok',
                data: image
            });

        } catch (err) {
            return next(err);
        }
    },

    index: async (req, res, next) => {
        try {
            const images = await Image.findAll();

            return res.status(200).json({
                status: true,
                message: 'ok',
                data: images
            });
            
        } catch (err) {
            return next(err);
        }
    },

    show: async (req, res, next) => {
        try {
            const { id } = req.params;

            const image = await Image.findOne({ where: { id } });
            if (!image) {
                return res.status(404).json({
                    status: false,
                    message: 'image not found!',
                    data: null
                });
            }

            return res.status(200).json({
                status: true,
                message: 'ok',
                data: image
            });

        } catch (err) {
            return next(err);
        }
    },

    destroy: async (req, res, next) => {
        try {
            const { id } = req.params;

            const image = await Image.findOne({ where: { id } });
            if (!image) {
                return res.status(404).json({
                    status: false,
                    message: 'image not found!',
                    data: null
                });
            }

            // delete imagekit file
            await imagekit.delete(image.file_id);

            const deleted = await Image.destroy({ where: { id } });

            return res.status(200).json({
                status: false,
                message: 'ok',
                data: {
                    affected: deleted
                }
            });

        } catch (err) {
            return next(err);
        }
    }
};