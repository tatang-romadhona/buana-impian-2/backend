const {
    JWT_SECRET
} = process.env;

const Validator = require('fastest-validator');
const v = new Validator();
const { User } = require('../../models');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { mailer } = require('../../util');

module.exports = {
    register: async (req, res, next) => {
        try {
            const schema = {
                f_name: 'string|required',
                l_name: 'string|required',
                email: 'email|required',
                password: 'string|required'
            };

            const errors = v.validate(req.body, schema);
            if (errors.length) {
                return res.status(400).json({
                    status: false,
                    message: 'bad request!',
                    data: errors
                });
            }

            const { email, password } = req.body;

            const exist = await User.findOne({ where: { email } });
            if (exist) {
                return res.status(409).json({
                    status: false,
                    message: 'user already exist',
                    data: null
                });
            }

            const encryptedPassword = await bcrypt.hash(password, 10);

            const user = await User.create({
                ...req.body,
                password: encryptedPassword,
                user_type: 'default'
            });

            const tokenData = {
                id: user.id,
                f_name: user.f_name,
                l_name: user.l_name,
                email: user.email
            };

            const secret = JWT_SECRET + user.password;
            const token = await jwt.sign(tokenData, secret, { expiresIn: '15m' });

            const link = `${req.protocol}://${req.get('host')}/activate-account/${user.id}?token=${token}`;
            const html = await mailer.loadHtml('activate-account.ejs', { name: user.f_name, link });
            await mailer.sendEmail(user.email, 'Activate Account', html);

            return res.status(201).json({
                status: true,
                message: 'ok',
                data: user
            });

        } catch (err) {
            return next(err);
        }
    },

    activateAccount: async (req, res, next) => {
        try {

        } catch (err) {
            return next(err);
        }
    },

    login: async (req, res, next) => {
        try {
            const schema = {
                email: 'email|required',
                password: 'string|required'
            };

            const errors = v.validate(req.body, schema);
            if (errors.length) {
                return res.status(400).json({
                    status: false,
                    message: 'bad request!',
                    data: errors
                });
            }

            const { email, password } = req.body;
            const user = await User.findOne({ where: { email } });
            if (!user) {
                return res.status(400).json({
                    status: false,
                    message: 'wrong email or password',
                    data: null
                });
            }

            const valid = await bcrypt.compare(password, user.password);
            if (!valid) {
                return res.status(400).json({
                    status: false,
                    message: 'wrong email or password',
                    data: null
                });
            }

            const tokenData = {
                id: user.id,
                f_name: user.f_name,
                l_name: user.l_name,
                email: user.email,
                password: user.password,
            };
            const token = await jwt.sign(tokenData, JWT_SECRET);

            return res.status(200).json({
                status: true,
                message: 'ok',
                data: { ...tokenData, token }
            });

        } catch (err) {
            return next(err);
        }
    },

    forgotPassword: async (req, res, next) => {
        try {
            const schema = {
                email: 'email|required',
            };

            const errors = v.validate(req.body, schema);
            if (errors.length) {
                return res.status(400).json({
                    status: false,
                    message: 'bad request!',
                    data: errors
                });
            }

            const { email } = req.body;
            const user = await User.findOne({ where: { email } });
            if (user) {
                const tokenData = {
                    id: user.id,
                    f_name: user.f_name,
                    l_name: user.l_name,
                    email: user.email
                };

                const secret = JWT_SECRET + user.password;
                const token = await jwt.sign(tokenData, secret, { expiresIn: '15m' });

                const link = `${req.protocol}://${req.get('host')}/reset-password/${user.id}?token=${token}`;
                const html = await mailer.loadHtml('welcome.ejs', { name: user.f_name, link });
                await mailer.sendEmail(user.email, 'Reset Password', html);
            }

            return res.json({
                status: true,
                message: 'email reset password akan dikirimkan jika email tersedia di dalam database.',
                data: null
            });

        } catch (err) {
            return next(err);
        }
    },

    resetPassword: async (req, res, next) => {
        try {

        } catch (err) {
            return next(err);
        }
    },
};