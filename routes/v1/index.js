const express = require('express');
const router = express.Router();

const { file, image, auth } = require('../../controllers/v1');
const { multer } = require('../../util');

// auth
router.post('/auth/register', auth.register);
router.post('/auth/login', auth.login);
router.post('/auth/forgot-password', auth.forgotPassword);
router.put('/auth/reset-password', auth.resetPassword);

// files
router.post('/files/upload', multer.fileSingle, file.store);
router.get('/files', file.index);
router.get('/files/:id', file.show);
router.delete('/files/:id', file.destroy);

// images
router.post('/images/upload', multer.imageSingle, image.store);
router.get('/images', image.index);
router.get('/images/:id', image.show);
router.delete('/images/:id', image.destroy);

module.exports = router;