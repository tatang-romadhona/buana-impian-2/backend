const { google } = require('googleapis');
const { client } = require('./oauth2');

const drive = google.drive({
    version: 'v3',
    auth: client
});

module.exports = {
    upload: (fileName, mimeType, fileData) => {
        return new Promise(async (resolve, reject) => {
            try {
                const response = await drive.files.create({
                    requestBody: {
                        name: fileName,
                        mimeType: mimeType
                    },
                    media: {
                        mimeType: mimeType,
                        body: fileData
                    }
                });

                return resolve(response.data);

            } catch (err) {
                return reject(err);
            }
        });
    },

    generatePublicLink: async (fileId) => {
        return new Promise(async (resolve, reject) => {
            try {
                await drive.permissions.create({
                    fileId,
                    requestBody: {
                        role: 'reader',
                        type: 'anyone'
                    }
                });

                const result = await drive.files.get({
                    fileId,
                    fields: 'webViewLink, webContentLink'
                });

                return resolve(result.data);

            } catch (err) {
                return reject(err);
            }
        });
    },

    delete: async (fileId) => {
        return new Promise(async (resolve, reject) => {
            try {
                const response = await drive.files.delete({ fileId });

                return resolve(response.data);

            } catch (err) {
                return reject(err);
            }
        });
    }
};
