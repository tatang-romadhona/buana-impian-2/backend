const {
    OAUTH_CLINET_ID,
    OAUTH_CLINET_SECRET,
    OAUTH_REFRESH_TOKEN,
    OAUTH_REDIRECT_URI,
} = process.env;

const { google } = require('googleapis');

const oauth2Client = new google.auth.OAuth2(
    OAUTH_CLINET_ID,
    OAUTH_CLINET_SECRET,
    OAUTH_REDIRECT_URI
);

oauth2Client.setCredentials({ refresh_token: OAUTH_REFRESH_TOKEN });

module.exports = {
    client: oauth2Client,
};
