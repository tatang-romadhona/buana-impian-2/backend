const {
    OAUTH_CLINET_ID,
    OAUTH_CLINET_SECRET,
    OAUTH_REFRESH_TOKEN,
    SENDER_EMAIL,
} = process.env;

const nodemailer = require('nodemailer');
const { client } = require('./oauth2');
const ejs = require('ejs');

module.exports = {
    sendEmail: (to, subject, html) => {
        return new Promise(async (resolve, reject) => {
            try {
                const accessToken = await client.getAccessToken();

                const transport = nodemailer.createTransport({
                    service: 'gmail',
                    auth: {
                        type: 'oauth2',
                        user: SENDER_EMAIL,
                        clientId: OAUTH_CLINET_ID,
                        clientSecret: OAUTH_CLINET_SECRET,
                        refreshToken: OAUTH_REFRESH_TOKEN,
                        accessToken
                    }
                });

                const mailOptions = { to, subject, html };
                const response = await transport.sendMail(mailOptions);

                return resolve(response);

            } catch (err) {
                return reject(err);
            }
        });
    },

    loadHtml: (fileName, data) => {
        return new Promise(async (resolve, reject) => {
            const path = __dirname + '/../../views/email/' + fileName;
            ejs.renderFile(path, data, (err, data) => {
                if (err) {
                    return reject(err);
                } else {
                    return resolve(data);
                }
            });
        });
    }
};